# -*- coding: us-ascii-unix -*-

NAME       = convergence-components
FULLNAME   = $(NAME)
VERSION    = 0.0.1
RELEASE    = $(NAME)-$(VERSION)
QT_PLATFORM_STYLE =
QT_PLATFORM_FALLBACK_STYLE =

clean:
	rm -rf dist
	rm -rf */.cache
	rm -rf */*/.cache
	rm -f */*.qmlc */*/*.qmlc
	rm -f platform

dist:
	$(MAKE) clean
	mkdir -p dist/$(RELEASE)
	cp -r `cat MANIFEST` dist/$(RELEASE)
	tar -C dist -cJf dist/$(RELEASE).tar.xz $(RELEASE)

install:
ifdef QT_PLATFORM_STYLE
	sed -i -e 's|# INSERT_PLATFORM_STYLE|export QT_QUICK_CONTROLS_STYLE="$$\{QT_QUICK_CONTROLS_STYLE:-$(QT_PLATFORM_STYLE)\}"|g' $(EXE) || true
endif
ifdef QT_PLATFORM_FALLBACK_STYLE
	sed -i -e 's|# INSERT_PLATFORM_FALLBACK_STYLE|export QT_QUICK_CONTROLS_FALLBACK_STYLE="$$\{QT_QUICK_CONTROLS_FALLBACK_STYLE:-$(QT_PLATFORM_FALLBACK_STYLE)\}"|g' $(EXE) || true
endif

platform-qtcontrols:
	rm -f platform || true
	ln -s platform.qtcontrols platform

platform-kirigami:
	rm -f platform || true
	ln -s platform.kirigami platform

platform-silica:
	rm -f platform || true
	ln -s platform.silica platform

platform-uuitk:
	rm -f platform || true
	ln -s platform.uuitk platform

.PHONY: clean dist install

