# ARCHIVED - NOT MAINTAINED 

It turns out I'm more into GNOME/GTK than KDE/Qt


# Convergence Components

Qt QML components to build convergent UIs for phones, desktops, the Web, etc.
Extracted from Pure Maps by Rinigus: https://github.com/rinigus/pure-maps

## Imports

Use for imports (as required)

```
import QtQuick 2.0
QtQuick.Layouts 1.1
import "."
import "platform"
```

## Notes

1. When defining width of the columns on the page, don't use anchors, but 

```
width: page.width
```

Otherwise, Kirigami platform may get to trouble.

2. Pages (PagePL) can have only one item. If some Connections, Timers, or 
similar is added, define them under the main item. This seems to be limitation 
of Kirigami `ScrollablePage`.
